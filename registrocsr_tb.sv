`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Tecnol�gico de Costa Rica
// Engineer: David Torres Garc�a
// 
// Create Date: 01/18/2020 07:21:36 PM
// Design Name: 
// Module Name: Mem_Datos_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module registrocsr_tb();
    reg write_enable, zeroflag; //Bits para habilitar escritura y la bandera de cero 
    wire out;                   //Dato de salida

    //Funci�n para escritura y lectura
    registrocsr dut(write_enable, zeroflag, out);   
   
   // Pruebas escritura y lectura:
   initial begin
   write_enable=1;  zeroflag=1;   #10;
   if (out !== zeroflag) $display("Error de escritura");
   
   write_enable=0;  zeroflag=0;   #10;
   if (out == zeroflag) $display("Error de lectura");
   
   write_enable=1;  zeroflag=0;   #10;
   if (out !== zeroflag) $display("Error de reset");
   
   end
endmodule
