`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/17/2020 05:59:33 PM
// Design Name: 
// Module Name: XOR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module XOR#(parameter width = 32)( // inicializa el modilo con 32 bits
input logic [width-1:0] a, //operando a
input logic [width-1:0] b, //operando b
output logic [width-1:0] y); //resultado
 
genvar i; 
generate 
 
    for(i=0; i<width; i=i+1) begin: forloop 
        assign y[i] = a[i] ^ b[i]; //operacion xor de a con b
    end 
endgenerate 
 
endmodule 
