`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/19/2020 07:01:44 PM
// Design Name: 
// Module Name: MUX_S
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module MUX_S#(parameter width = 32)(
input logic [width-1:0] d0, d1,d2,d3,d4,d5,d6,d7,
input logic [2:0]sel,
output logic [width-1:0] y
);
//             sel[2]   __________________1__________________________          _____________________0__________________
//             sel[1]              _________1_____   _________0_____                     _____1___________    __________0_____
//             sel[0]                       1  0               1  0                                1    0              1    0
assign y =     sel[2] ? (sel[1] ? (sel[0] ? d7:d6 ):(sel[0] ? d5:d4 ))    :    (sel[1] ? (sel[0] ? d3:d2) : (sel[0] ? d1:d0));
endmodule
