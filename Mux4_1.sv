`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2020 05:07:53 PM
// Design Name: 
// Module Name: Mux4_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mux4_1(
input logic [31:0] d0, d1, d2, d3, 
input logic [1:0]sel, 
output logic [31:0] y 
); 
 
                  //sel[1]_____1_____  _____0______
                  //sel[0]  __1__0__            __1__0__
assign y = sel[1] ? (sel[0] ? d3:d2) : (sel[0] ? d1:d0);
endmodule