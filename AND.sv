`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/17/2020 05:36:08 PM
// Design Name: 
// Module Name: AND
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module AND#(parameter width = 32)( 
input logic [width-1:0] a,
input logic [width-1:0] b, 
output logic [width-1:0] y);

genvar i;
generate
    for(i=0; i<width; i=i+1) begin: forloop
        assign y[i] = a[i] & b[i];
    end
endgenerate

endmodule

