`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.01.2020 23:22:12
// Design Name: 
// Module Name: tb_MUX_S
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_MUX_S();
reg [31:0] d0, d1,d2,d3,d4,d5,d6,d7;
reg [2:0]sel;
reg [31:0]y;

// instantiate device under test
MUX_S#(32) dut (d0, d1,d2,d3,d4,d5,d6,d7, sel, y);

// 
initial begin
//CASO 1
d0 = 32'h00000000; d1 = 32'h00000001; d2 = 32'h00000010; d3 = 32'h00000011; d4 = 32'h00000100; d5 = 32'h00000101; d6 = 32'h00000110; d7 = 32'h00000111;
sel = 3'b000; #10;                   // apply input, wait
if (y !== 'h00000000) $display("Caso 1 fallo");     // check
//CASO 2                
d0 = 32'h00000000; d1 = 32'h00000001; d2 = 32'h00000010; d3 = 32'h00000011; d4 = 32'h00000100; d5 = 32'h00000101; d6 = 32'h00000110; d7 = 32'h00000111;
sel = 3'b100; #10;                   // apply input, wait
if (y !== 'h00000100) $display("Caso 1 fallo");     // check
//CASO 3
d0 = 32'h00000000; d1 = 32'h00000001; d2 = 32'h00000010; d3 = 32'h00000011; d4 = 32'h00000100; d5 = 32'h00000101; d6 = 32'h00000110; d7 = 32'h00000111;
sel = 3'b111; #10;                   // apply input, wait
if (y !== 'h00000111) $display("Caso 1 fallo");     // check

end

endmodule
