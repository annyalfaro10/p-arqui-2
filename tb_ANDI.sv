`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.01.2020 18:08:30
// Design Name: 
// Module Name: tb_ADDI
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_ANDI();
reg [11:0]a, b;
reg [11:0]y;

// instantiate device under test
ANDI dut(.a(a), .b(b), .y(y));

// 
initial begin
//CASO 1
a = 32'h11111111; b = 12'h111; #10;                   // apply input, wait
if (y !== 32'h11111111) $display("Caso 1 fallo");     // check
//CASO 2                
b = 12'h000; #10;                                // apply input, wait
if (y !== 32'h00000000) $display("Caso 2 fallo");     // check
//CASO 3
a = 32'h01010101; b = 12'h010; #10;                   // apply input, wait
if (y !== 32'h00000000 ) $display("Caso 3 fallo");    // check

end
endmodule
