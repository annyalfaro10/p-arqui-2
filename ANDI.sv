`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/17/2020 05:45:53 PM
// Design Name: 
// Module Name: ANDI
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ANDI( 
input logic [31:0] a, //operando a
input logic [11:0] b, //inmediato b
output logic [31:0] y); //resultado

assign b[31:12]='b0; // rellena  los bits del inmediato

genvar i; //variable para recorrer los bits 
generate
    for(i=0; i<12; i=i+1) begin: forloop
        assign y[i] = a[i] & b[i]; //operacion de and
    end
endgenerate

endmodule


