`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/18/2020 05:12:05 PM
// Design Name: 
// Module Name: Mem_Datos
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mem_Datos(
    input clk, write_enable, //reloj (escribe en flanco negativo), se�al para habilitar escritura
    input [5:0] addr, //Direccion para accesar (se usa para leer y escribir)
    input logic [31:0] din, //Dato de entrada
    output logic [31:0] dout //Dato de salida
    );

//espacio de memoria
reg [31:0] memspace [63:0];

//pone a la salida la direccion buscada
assign dout = memspace[addr];

//si la se�al de write enable esta activa entonces guarda el dato
always @(negedge clk)
begin
    if (write_enable)
    begin
        memspace[addr] <= din;
    end
end    
endmodule
