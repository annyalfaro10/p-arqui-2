`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.01.2020 20:30:30
// Design Name: 
// Module Name: tb_OR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_OR();
reg [31:0] a, b;
reg [31:0] y;

// instantiate device under test
OR#(32) dut (a, b, y);

// 
initial begin
//CASO 1
a = 32'h11111111; b = 32'h11111111; #10;                   // apply input, wait
if (y !== 32'h11111111) $display("Caso 1 fallo");     // check
//CASO 2                
b = 32'h00000000; #10;                                // apply input, wait
if (y !== 32'h11111111) $display("Caso 2 fallo");     // check
//CASO 3
a = 32'h01010101; b = 32'h10101010; #10;                   // apply input, wait
if (y !== 32'h11111111 ) $display("Caso 3 fallo");    // check

end
endmodule

