`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.01.2020 17:28:02
// Design Name: 
// Module Name: tb_EXT_ZEROO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_EXT_ZEROO();
reg [3:0] a;
reg [31:0] y;

// instantiate device under test
EXT_ZERO#(4) dut (a, y);

// 
initial begin
//CASO 1
a = 4'b1111; #10;                   // apply input, wait
if (y !== 'h0000000f) $display("Caso 1 fallo");     // check
//CASO 2                
a = 4'h0; #10;                                // apply input, wait
if (y !== 'h00000000) $display("Caso 2 fallo");     // check
//CASO 3
a = 4'b0001; #10;                   // apply input, wait
if (y !== 'h00000001 ) $display("Caso 3 fallo");    // check

end
endmodule
