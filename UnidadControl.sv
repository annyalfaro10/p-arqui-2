`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/20/2020 10:06:04 AM
// Design Name: 
// Module Name: UnidadControl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UnidadControl(
    input [6:0] opcode,
    input [2:0] func3,
    output logic regWrite,
    output logic [1:0]ALUSrc, // selector para el mux de operandos
    output logic [2:0] ALUop, // selector de la operacion de la ALU
    output logic memRead, 
    output logic memWrite,
    output logic memToReg,
    output logic branch
    );
    
    always @(*)
    begin
        case(opcode) 
        7'b1100011: // BEQ, BNE
            begin
            ALUSrc = 2'b00; // registro como operando b
            memToReg = 1'b0; 
            regWrite = 1'b0;
            memRead = 1'b0;
            memWrite = 1'b0;
            branch = 1'b1;
            ALUop = 3'b010; // usa XOR para comparar los registros si son 0 se levanta la bandera cero y se cumple el branch
            end
        7'b0000011:  // LW
            begin
            ALUSrc = 2'b01; // usa el inmediato como operando b
            memToReg = 1'b1;
            regWrite = 1'b1;
            memRead = 1'b1;
            memWrite = 1'b0;
            branch = 1'b0;
            ALUop = 3'b011; // usa ADD para la suma del registro base con el inmediato
            end
            
        7'b0100011:  // SW
            begin
            ALUSrc = 2'b01; // usa el inmediato como operando b
            memToReg = 1'b0;
            regWrite = 1'b0;
            memRead = 1'b0;
            memWrite = 1'b1;
            branch = 1'b0;
            ALUop = 3'b011; // usa ADD para la suma del registro base con el inmediato
            end
            
        7'b0010011:  // ADDI, SLTIU, XOR, ORI, ANDI, SLLI, SRLI
            begin
            memToReg = 1'b0;
            regWrite = 1'b1;
            memRead = 1'b0;
            memWrite = 1'b0;
            branch = 1'b0;
            case(func3) 
                0: begin 
                    ALUop = 3'b000; //ANDI
                    ALUSrc = 2'b01; // usa el inmediato como operando b
                    end
                1: begin
                    ALUop = 3'b001; //ORI
                    ALUSrc = 2'b01; // usa el inmediato como operando b
                    end
                2: begin 
                    ALUop = 3'b010; //XORI
                    ALUSrc = 2'b01; // usa el inmediato como operando b
                    end
                3: begin
                    ALUop = 3'b011; //ADDI
                    ALUSrc = 2'b01; // usa el inmediato como operando b
                    end
                4: begin
                    ALUop = 3'b100; //SLLI
                    ALUSrc = 2'b10; // usa el inmediato como operando b
                    end
                5: begin
                    ALUop = 3'b101; //SRLI
                    ALUSrc = 2'b10; // usa el inmediato como operando b
                    end
                6: begin 
                    ALUop = 3'b110; //SLTIU
                    ALUSrc = 2'b01; // usa el inmediato como operando b
                    end
                default: begin
                    ALUop = 3'b111;
                    ALUSrc = 2'b01; // usa el inmediato como operando b
                    end
            endcase
            end
            
         7'b0110011:  // AND, OR, XOR, ADD, SLL, SRL, SLTU
            begin
            ALUSrc = 2'b00; //Usa registro como operando b
            memToReg = 1'b0;
            regWrite = 1'b1;
            memRead = 1'b0;
            memWrite = 1'b0;
            branch = 1'b0;
            case(func3) 
                0: ALUop = 3'b000; //AND
                1: ALUop = 3'b001; //OR
                2: ALUop = 3'b010; //XOR
                3: ALUop = 3'b011; //ADD
                4: ALUop = 3'b100; //SLL
                5: ALUop = 3'b101; //SRL
                6: ALUop = 3'b110; //SLTU
                default: ALUop = 3'b111;
            endcase
            end
         7'b0101011:  // CSRRW, CSRRWI
            begin
            ALUSrc = 1'b0;
            memToReg = 1'b0;
            regWrite = 1'b1;
            memRead = 1'b0;
            memWrite = 1'b0;
            branch = 1'b0;
            ALUop = 2'b10; 
            end   
         
         default: begin
            ALUSrc = 1'b0;
            memToReg = 1'b0;
            regWrite = 1'b0;
            memRead = 1'b0;
            memWrite = 1'b0;
            branch = 1'b0;
            ALUop = 2'b00;
            end
         endcase
         end
    
endmodule
