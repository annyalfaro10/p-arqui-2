`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Tecnol�gico de Costa Rica
// Engineer: David Torres Garc�a
// 
// Create Date: 01/18/2020 09:38:19 AM
// Design Name: 
// Module Name: RegFile_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RegFile_tb();
    reg clk, write_enable;    //cable para reloj, write enable en 1 si quiere escribir
    reg [4:0] readreg1;       //Son las direcciones de lectura para reg 1
    reg [4:0] readreg2;       //Son las direcciones de lectura para reg 2
    reg [4:0] writereg;       //Son las direcciones de escritura
    reg [31:0] datain;        //Entrada de datos
    wire [31:0] dataout1; //salida de datos 1
    wire [31:0] dataout2;  //salida de datos 2

    //Funci�n para escritura (sin los registros de escritura y sin salidas)
    RegFile dut(clk, write_enable, readreg1, readreg2, writereg, datain, dataout1, dataout2);   
          
    //Se inicializa el relog que usa la funci�n
    always                              // no sensitivity list, so it always executes
        begin
            clk = 1; #5; clk = 0; #5;   // 10ns period
        end
    
   // Pruebas escritura y lectura:
   initial begin
   write_enable=1; writereg=5'b00000; datain=32'h00000300; #10;
   write_enable=1; writereg=5'b00001; datain=32'h00a00000; #10;
   write_enable=0; readreg1=5'b00000; readreg2=5'b00001;   #10;
   if (dataout1 !== 32'h00000000) $display("Error: R0 no se mantiene en 0");
   if (dataout2 !== 32'h00a00000) $display("Error de escritura R1");
   
   write_enable=1; writereg=5'b00010; datain=32'h00000305; #10;
   write_enable=1; writereg=5'b00011; datain=32'h00700300; #10;
   write_enable=0; readreg1=5'b00010; readreg2=5'b00011;   #10;
   if (dataout1 !== 32'h00000305) $display("Error de escritura R2");
   if (dataout2 !== 32'h00700300) $display("Error de escritura R3");   
   
   write_enable=1; writereg=5'b00100; datain=32'h00000020; #10;
   write_enable=1; writereg=5'b00101; datain=32'h50000300; #10;
   write_enable=0; readreg1=5'b00100; readreg2=5'b00101;   #10;
   if (dataout1 !== 32'h00000020) $display("Error de escritura R4");
   if (dataout2 !== 32'h50000300) $display("Error de escritura R5");
   
   write_enable=1; writereg=5'b00110; datain=32'h00e00300; #10;
   write_enable=1; writereg=5'b00111; datain=32'h0f000300; #10;
   write_enable=0; readreg1=5'b00110; readreg2=5'b00111;   #10;
   if (dataout1 !== 32'h00e00300) $display("Error de escritura R6");
   if (dataout2 !== 32'h0f000300) $display("Error de escritura R7");
   
   write_enable=1; writereg=5'b01000; datain=32'hee000220; #10;
   write_enable=1; writereg=5'b01001; datain=32'h0ff00307; #10;
   write_enable=0; readreg1=5'b01000; readreg2=5'b01001;   #10;
   if (dataout1 !== 32'hee000220) $display("Error de escritura R8");
   if (dataout2 !== 32'h0ff00307) $display("Error de escritura R9");
   
   write_enable=1; writereg=5'b01010; datain=32'h00300303; #10;
   write_enable=1; writereg=5'b01011; datain=32'h0a0d0300; #10;
   write_enable=0; readreg1=5'b01010; readreg2=5'b01011;   #10;
   if (dataout1 !== 32'h00300303) $display("Error de escritura R10");
   if (dataout2 !== 32'h0a0d0300) $display("Error de escritura R11");
   
   write_enable=1; writereg=5'b01100; datain=32'h00040300; #10;
   write_enable=1; writereg=5'b01101; datain=32'h0a000301; #10;
   write_enable=0; readreg1=5'b01100; readreg2=5'b01101;   #10;
   if (dataout1 !== 32'h00040300) $display("Error de escritura R12");
   if (dataout2 !== 32'h0a000301) $display("Error de escritura R13");
   
   write_enable=1; writereg=5'b01110; datain=32'h00200300; #10;
   write_enable=1; writereg=5'b01111; datain=32'h00070300; #10;
   write_enable=0; readreg1=5'b01110; readreg2=5'b01111;   #10;
   if (dataout1 !== 32'h00200300) $display("Error de escritura R14");
   if (dataout2 !== 32'h00070300) $display("Error de escritura R15");
   
   write_enable=1; writereg=5'b10000; datain=32'h70000300; #10;
   write_enable=1; writereg=5'b10001; datain=32'h0a00030a; #10;
   write_enable=0; readreg1=5'b10000; readreg2=5'b10001;   #10;
   if (dataout1 !== 32'h70000300) $display("Error de escritura R16");
   if (dataout2 !== 32'h0a00030a) $display("Error de escritura R17");
   
   write_enable=1; writereg=5'b10010; datain=32'h00c0a300; #10;
   write_enable=1; writereg=5'b11001; datain=32'h0f00030f; #10;
   write_enable=0; readreg1=5'b10010; readreg2=5'b11001;   #10;
   if (dataout1 !== 32'h00c0a300) $display("Error de escritura R18");
   if (dataout2 !== 32'h0f00030f) $display("Error de escritura R25");
   
   write_enable=1; writereg=5'b11010; datain=32'h000e0f00; #10;
   write_enable=1; writereg=5'b11111; datain=32'h00200320; #10;
   write_enable=0; readreg1=5'b11010; readreg2=5'b11111;   #10;
   if (dataout1 !== 32'h000e0f00) $display("Error de escritura R26");
   if (dataout2 !== 32'h00200320) $display("Error de escritura R32");
   end
     
endmodule