`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/14/2020 10:38:26 PM
// Design Name: 
// Module Name: RegFile
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//HAY QUE INICIALIZAR LOS REGISTROS
module RegFile(
    input clk, write_enable, //cable para reloj, write enable en 1 si quiere escribir
    input [4:0] readreg1, //Son las direcciones de lectura para reg 1
    input [4:0] readreg2, //Son las direcciones de lectura para reg 2
    input [4:0] writereg, //Son las direcciones de escritura
    input [31:0] datain, //Entrada de datos
    output reg [31:0] dataout1, //salida de datos 1
    output reg [31:0] dataout2 //salida de datos 1
    );
//SI LA SIMULACION TIRA ERROR QUE DICE QUE SE ASIGNAN UNPACKED A PACKED
// ES PORQUE TIENE QUE SEGUIR LA CONVENCION DE DETALLAR LOS CABLES COMO [CANT CABLES-1:0] NOMBRE DE CABLES

//Son 32 registros (registers [0:31]) de 32 bits
reg [31:0] registers [31:0];

//Pone en las salida de datos lo que contenga el registro pedido
assign dataout1 = registers[readreg1];
assign dataout2 = registers[readreg2];
assign registers[0] = 32'b0000_0000_0000_0000_0000_0000_0000_0000;
//escribe en el lado positivo del reloj
always @ (negedge clk)
begin
    if (write_enable)
    begin
        if(writereg ==0)
            registers[0] = 32'b0000_0000_0000_0000_0000_0000_0000_0000;
        else   
            registers[writereg] <= datain;
    end
end    
    
endmodule
