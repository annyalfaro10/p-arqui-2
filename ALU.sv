`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/19/2020 05:57:13 PM
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU #(parameter int unsigned size = 32)(
    input [2:0]alucontrol, //seleccion de cual es la operacion que se hace dentro de la alu
    input [size-1:0] a, //operando a
    input [size-1:0] b, //operando b
    output [size-1:0]resultado,
    output zero //bandera de cero
    );
    
///////////////////////////////////////cableado///////////////////////////////////
//wire and
wire [size-1:0] w_and;

//wire or
wire [size-1:0] w_or;

//wire xor
wire [size-1:0] w_xor;

//wire add
wire [size-1:0] w_add;

//wire sll
wire [size-1:0] w_sll;

//wire srl
wire [size-1:0] w_srl;

//wire sltu
wire [size-1:0] w_sltu;



///////////////////////////////////////////////////////////////////////////////////////////
//banderas

// zero
NOR #(size) bandzero(resultado,32'b0,zero);

/////////////////////////////////Operaciones///////////////////////////////////////////////////////////

//AND
AND #(size) operand(a,b,w_and);

//OR
OR #(size) operor(a,b,w_or);

//XOR 
XOR #(size) operxor (a,b,w_xor);

//ADD
ADD #(size) operadd (a,b,w_add);

//SLL
SLL #(size) opersll (a,b,w_sll);

//SRL
SRL #(size) opersrl (a,b,w_srl);

//SLTU
SLTU #(size) opersltu (a,b,w_sltu);

////////////////////////////////////////////////////////////////////////////////////////////

//mux para seleccionar la salida

MUX_S #(size) mux_salida(w_and,w_or,w_xor,w_add,w_sll,w_srl,w_sltu,'b0,alucontrol,resultado);

/////////////////////////////////////////////////////////////////////////////////////////

endmodule
