`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Tecnol�gico de Costa Rica
// Engineer: David Torres Garc�a
// 
// Create Date: 01/18/2020 07:21:36 PM
// Design Name: 
// Module Name: Mem_Datos_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reg_PC_tb();
    reg clk, reset;     //cable para reloj, write enable en 1 si quiere escribir
    reg [5:0] in;       //Dato de entrada
    wire [5:0] out;     //Dato de salida

    //Funci�n para escritura y lectura
    reg_PC dut(clk, reset, in, out);   
          
    //Se inicializa el reloj que usa la funci�n
    always
        begin
            clk = 1; #5; clk = 0; #5;   // 10ns por per�odo
        end
    
   // Pruebas escritura y lectura:
   initial begin
   reset=1;     in=6'b001000;   #10;
   if (out !== 6'b000000) $display("Error de reset");
   
   reset=1;     in=6'b000110;   #10;
   if (out !== 6'b000000) $display("Error de reset");
   
   reset=1;     in=6'b100000;   #10;
   if (out !== 6'b000000) $display("Error de reset");
   
   reset=0;     in=6'b010000;   #10;
   if (out !== 6'b010000) $display("Error de salida");
   
   reset=0;     in=6'b100011;   #10;
   if (out !== 6'b100011) $display("Error de salida");
   
   reset=0;     in=6'b111111;   #10;
   if (out !== 6'b111111) $display("Error de salida");
   
   reset=1;     in=6'b111101;   #10;
   if (out !== 6'b000000) $display("Error de reset");
   end
endmodule
