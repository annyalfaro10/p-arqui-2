`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/16/2020 10:20:22 AM
// Design Name: 
// Module Name: Mem_Inst
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mem_Inst (
    input [5:0] address, //direccion de memoria, son 64 espacios
    output logic [31:0] data //datos de salida 32 bits
    );

//se asignan datos a la memoria ya que es una ROM
reg [31:0] shapemem[63:0];
//1
assign shapemem[0] = 'h00000533;//and
assign shapemem[1] = 'h000005B3;
assign shapemem[2] = 'h00000633;
assign shapemem[3] = 'h000006B3;

assign shapemem[4] = 'h00000733;
assign shapemem[5] = 'h000007B3;
assign shapemem[6] = 'h00000833;
assign shapemem[7] = 'h000008B3;

assign shapemem[8] = 'h0002503;//lw
assign shapemem[9] = 'h06403593;//addi
//2
assign shapemem[10] = 'h00B02633;//xor
assign shapemem[11] = 'h00C59863;//bne
assign shapemem[12] = 'h00001713;//ori
assign shapemem[13] = 'h00004793;//slli
assign shapemem[14] = 'h00F00A33;//and
assign shapemem[15] = 'h00006AB3;//sltiu
assign shapemem[16] = 'h0005A013;//xori
assign shapemem[17] = 'h00B035B3;//add
assign shapemem[18] = 'h00C04633;//sll
assign shapemem[19] = 'h00C066B3;//sltu
//3
assign shapemem[20] = 'h00005713;//srli
assign shapemem[21] = 'h00100793;//andi
assign shapemem[22] = 'h00F01833;//or
assign shapemem[23] = 'h00C5DCE3;//srl
assign shapemem[24] = 'h00B05533;//sw
assign shapemem[25] = 'h00501003;//beq
assign shapemem[26] = 'h0000_0000;
assign shapemem[27] = 'h0000_0000;
assign shapemem[28] = 'h0000_0000;
assign shapemem[29] = 'h0000_0000;
//4
assign shapemem[30] = 'h0000_0000;
assign shapemem[31] = 'h0000_0000;
assign shapemem[32] = 'h0000_0000;
assign shapemem[33] = 'h0000_0000;
assign shapemem[34] = 'h0000_0000;
assign shapemem[35] = 'h0000_0000;
assign shapemem[36] = 'h0000_0000;
assign shapemem[37] = 'h0000_0000;
assign shapemem[38] = 'h0000_0000;
assign shapemem[39] = 'h0000_0000;
//5
assign shapemem[40] = 'h0000_0000;
assign shapemem[41] = 'h0000_0000;
assign shapemem[42] = 'h0000_0000;
assign shapemem[43] = 'h0000_0000;
assign shapemem[44] = 'h0000_0000;
assign shapemem[45] = 'h0000_0000;
assign shapemem[46] = 'h0000_0000;
assign shapemem[47] = 'h0000_0000;
assign shapemem[48] = 'h0000_0000;
assign shapemem[49] = 'h0000_0000;
//6
assign shapemem[50] = 'h0000_0000;
assign shapemem[51] = 'h0000_0000;
assign shapemem[52] = 'h0000_0000;
assign shapemem[53] = 'h0000_0000;
assign shapemem[54] = 'h0000_0000;
assign shapemem[55] = 'h0000_0000;
assign shapemem[56] = 'h0000_0000;
assign shapemem[57] = 'h0000_0000;
assign shapemem[58] = 'h0000_0000;
assign shapemem[59] = 'h0000_0000;

assign shapemem[60] = 'h0000_0000;
assign shapemem[61] = 'h0000_0000;
assign shapemem[62] = 'h0000_0000;
assign shapemem[63] = 'h0000_0000;

//asigna en la salida el dato buscado
assign data = shapemem[address];
endmodule
