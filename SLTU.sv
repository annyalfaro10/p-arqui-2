`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/19/2020 02:11:15 PM
// Design Name: 
// Module Name: SLTU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module SLTU #(parameter int unsigned width = 32)( 
    input wire [width-1:0] a,
    input wire [width-1:0] b,
    output reg [width-1:0]y //1 si a < b (rs1 < rs2) y 0 para lo demas
    
    );
    logic lower;
    always @* begin
      if (a<b) begin //condicion que a es menor que b
        lower = 1;
      end
      else begin //  resto de posibilidades
        lower = 0;
      end
    end
    assign y[width-1:1]=31'b0;
    assign y[0]=lower;
endmodule