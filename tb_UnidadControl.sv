`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.01.2020 17:43:27
// Design Name: 
// Module Name: tb_UnidadControl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_UnidadControl();
reg [6:0] opcode;
reg [2:0] func3;
reg regWrite;
reg [1:0]ALUSrc;
reg [2:0] ALUop;
reg memRead;
reg memWrite;
reg memToReg;
reg branch;


// instantiate device under test
UnidadControl dut (opcode,func3, regWrite, ALUSrc, ALUop, memRead, memWrite, memToReg, branch);

// 
initial begin
//CASO 1 (BEQ, BNE)
opcode = 7'b1100011;func3=3'b000; #10;                   // apply input, wait
if (regWrite !== 1'b0 ) $display("Caso 1 fallo regWrite");     // check
if (ALUSrc !== 2'b00) $display("Caso 1 fallo ALUSrc");     // check
if (ALUop !== 3'b010) $display("Caso 1 fallo ALUop");     // check
if (memRead !== 1'b0 ) $display("Caso 1 fallo memRead");     // check
if (memWrite !== 1'b0 ) $display("Caso 1 fallo memWrite");     // check
if (memToReg !== 1'b0 ) $display("Caso 1 fallo memToReg");     // check
if (branch !== 1'b1 ) $display("Caso 1 fallo branch");     // check         

//CASO 2   // ADDI, SLTIU, XOR, ORI, ANDI, SLLI, SRLI  (con ORI)           
opcode = 7'b0010011; func3=3'b001; #10;                   // apply input, wait
if (regWrite !== 1'b1 ) $display("Caso 2 fallo regWrite");     // check
if (ALUSrc !== 2'b01 ) $display("Caso 2 fallo ALUSrc");     // check
if (ALUop !== 3'b001 ) $display("Caso 2 fallo ALUop");     // check
if (memRead !== 1'b0 ) $display("Caso 2 fallo memRead");     // check
if (memWrite !== 1'b0 ) $display("Caso 2 fallo memWrite");     // check
if (memToReg !== 1'b0 ) $display("Caso 2 fallo memToReg");     // check
if (branch !== 1'b0 ) $display("Caso 2 fallo branch");     // check    

//CASO 3  // CSRRW, CSRRWI
opcode = 7'b0101011; func3=3'b000; #10;                   // apply input, wait
if (regWrite !== 1'b1 ) $display("Caso 1 fallo regWrite");     // check
if (ALUSrc !== 1'b0 ) $display("Caso 1 fallo ALUSrc");     // check
if (ALUop !== 2'b10 ) $display("Caso 1 fallo ALUop");     // check
if (memRead !== 1'b0 ) $display("Caso 1 fallo memRead");     // check
if (memWrite !== 1'b0 ) $display("Caso 1 fallo memWrite");     // check
if (memToReg !== 1'b0 ) $display("Caso 1 fallo memToReg");     // check
if (branch !== 1'b0 ) $display("Caso 1 fallo branch");     // check    

end

endmodule
