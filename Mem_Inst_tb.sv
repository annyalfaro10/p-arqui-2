`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Tecnol�gico de Costa Rica
// Engineer: David Torres Garc�a
// 
// Create Date: 01/18/2020 07:19:35 PM
// Design Name: 
// Module Name: Mem_Inst_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mem_Inst_tb();           
    reg [5:0] address;  //L�neas para la direcci�n de la instrucci�n.
    wire [31:0] data;   //Datos de la instrucci�n seleccionada.

    //Funci�n para la lectura de las instrucciones.
    Mem_Inst dut(address, data);   
          
    
   // Pruebas escritura y lectura:
   initial begin
   address=6'd0;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 0");
   
   address=6'd1;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 1");
   
   address=6'd2;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 2");
   
   address=6'd3;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 3");
   
   address=6'd4;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 4");
   
   address=6'd5;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 5");
   
   address=6'd6;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 6");
   
   address=6'd7;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 7");
   
   address=6'd8;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 8");
   
   address=6'd9;    #10;
   if (data !== 32'h0000_0000) $display("Error de lectura en Addr: 9");
   
   address=6'd10;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 10");
   
   address=6'd11;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 11");
   
   address=6'd12;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 12");
   
   address=6'd13;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 13");
   
   address=6'd14;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 14");
   
   address=6'd15;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 15");
   
   address=6'd16;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 16");
   
   address=6'd17;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 17");
   
   address=6'd18;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 18");
   
   address=6'd19;    #10;
   if (data !== 32'h0000_000A) $display("Error de lectura en Addr: 19");
   
   address=6'd20;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 20");
   
   address=6'd21;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 21");
   
   address=6'd22;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 22");
   
   address=6'd23;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 23");
   
   address=6'd24;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 24");
   
   address=6'd25;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 25");
   
   address=6'd26;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 26");
   
   address=6'd27;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 27");
   
   address=6'd28;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 28");
   
   address=6'd29;    #10;
   if (data !== 32'h0000_0070) $display("Error de lectura en Addr: 29");
   
   address=6'd30;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 30");
   
   address=6'd31;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 31");
   
   address=6'd32;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 32");
   
   address=6'd33;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 33");
   
   address=6'd34;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 34");
   
   address=6'd35;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 35");
   
   address=6'd36;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 36");
   
   address=6'd37;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 37");
   
   address=6'd38;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 38");
   
   address=6'd39;    #10;
   if (data !== 32'h0000_0004) $display("Error de lectura en Addr: 39");
   
   address=6'd40;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 40");
   
   address=6'd41;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 41");
   
   address=6'd42;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 42");
   
   address=6'd43;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 43");
   
   address=6'd44;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 44");
   
   address=6'd45;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 45");
   
   address=6'd46;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 46");
   
   address=6'd47;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 47");
   
   address=6'd48;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 48");
   
   address=6'd49;    #10;
   if (data !== 32'h0000_00C0) $display("Error de lectura en Addr: 49");
   
   address=6'd50;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 50");
   
   address=6'd51;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 51");
   
   address=6'd52;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 52");
   
   address=6'd53;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 53");
   
   address=6'd54;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 54");
   
   address=6'd55;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 55");
   
   address=6'd56;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 56");
   
   address=6'd57;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 57");
   
   address=6'd58;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 58");
   
   address=6'd59;    #10;
   if (data !== 32'h0000_0009) $display("Error de lectura en Addr: 59");
   
   address=6'd60;    #10;
   if (data !== 32'h0000_0010) $display("Error de lectura en Addr: 60");
   
   address=6'd61;    #10;
   if (data !== 32'h0000_0010) $display("Error de lectura en Addr: 61");
   
   address=6'd62;    #10;
   if (data !== 32'h0000_0010) $display("Error de lectura en Addr: 62");
   
   address=6'd63;    #10;
   if (data !== 32'h0000_0010) $display("Error de lectura en Addr: 63");
   
   end

endmodule
