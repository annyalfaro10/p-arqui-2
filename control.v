module control(
	input clk,rst, p_error,
	input [5:0] cmd_in,
	output reg aluin_reg_en, datain_reg_en, aluout_reg_en, invalid_data,
	output reg [1:0] in_select_a, in_select_b,
	output reg [3:0] opcode
);

localparam DECODE = 0;
localparam EXE = 1;
localparam WB = 2; 

reg [1:0] opcode_coded;
reg state = 0; 
reg next_state = 0;
always@(posedge clk) begin
	if(rst|p_error) begin
		state <= 0; 
		next_state <= 0;
	end
	else
		state <= next_state;
end

always@(*) begin
	next_state = state;
		case(state) 
			DECODE: begin
				datain_reg_en = 1;
				aluin_reg_en = 0;
				aluout_reg_en = 1;
				invalid_data = 0;
				//in_select_a = cmd_in[5:4];
				//in_select_b = cmd_in[3:2];
				//opcode_coded = cmd_in[1:0];
				next_state = EXE;
				end
			EXE: begin
				datain_reg_en = 0;
				aluin_reg_en = 1;
				aluout_reg_en = 0;
				invalid_data = 0;
				in_select_a = cmd_in[5:4];
				in_select_b = cmd_in[3:2];
				opcode_coded = cmd_in[1:0];
				next_state = WB;
				end
			WB: begin
				datain_reg_en = 0;
				aluin_reg_en = 0;
				aluout_reg_en = 1;
				invalid_data = 0;
				//in_select_a = cmd_in[5:4];
				//in_select_b = cmd_in[3:2];
				//opcode_coded = cmd_in[1:0];
				next_state = DECODE;
				end
		endcase
		case(opcode_coded)
			2'b00: opcode = 4'd0;
			2'b01: opcode = 4'd2;
			2'b10: opcode = 4'd4;
			2'b11: opcode = 4'd8;
			default: opcode = 0;
		endcase
	end
endmodule



