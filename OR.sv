`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/18/2020 06:52:03 PM
// Design Name: 
// Module Name: OR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module OR #(parameter width = 32)(
    input logic [width-1:0] a,  //operando a
    input logic [width-1:0] b, //operando b
    output logic [width-1:0] y); //resultado


genvar i; //variable para recorrer los bits 
generate
    for(i=0; i<width; i=i+1) begin: forloop
        assign y[i] = a[i] | b[i]; //operacion de or
    end
endgenerate

endmodule

