`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.01.2020 21:43:08
// Design Name: 
// Module Name: tb_SLTU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_SLTU();
reg [31:0] a, b;
reg [31:0] y;

// instantiate device under test
SLTU#(32) dut (a, b, y);

// 
initial begin
//CASO 1
a = 32'h00000000; b = 32'h11111111; #10;                   // apply input, wait
if (y !== 'h00000001) $display("Caso 1 fallo");     // check
//CASO 2                
b = 32'h00000000; #10;                                // apply input, wait
if (y !== 'h00000000) $display("Caso 2 fallo");     // check
//CASO 3
a = 32'h000001F4; b = 32'h000003E8; #10;                   // apply input, wait
if (y !== 'h00000001 ) $display("Caso 3 fallo");    // check

end

endmodule
