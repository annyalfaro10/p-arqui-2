`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/18/2020 04:36:12 PM
// Design Name: 
// Module Name: SRL
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SRL #(parameter width = 32)(
    input logic [width-1:0] num, //numero al que se le hace el corrimiento
    input logic [width-1:0] shamt, //cantidad de corrimientos
    output logic [width-1:0] num_shift // numero corrido
    );

assign num_shift = num >> shamt;

endmodule

