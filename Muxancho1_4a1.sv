`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2020 10:44:30 PM
// Design Name: 
// Module Name: Muxancho1_4a1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Muxancho1_4a1(
input logic d0, d1, d2, d3, 
input logic [1:0]sel, 
output logic y 
); 
 
                  //sel[1]_____1_____  _____0______
                  //sel[0]  __1__0__            __1__0__
assign y = sel[1] ? (sel[0] ? d3:d2) : (sel[0] ? d1:d0);
endmodule
