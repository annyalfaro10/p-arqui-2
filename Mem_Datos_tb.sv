`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Tecnol�gico de Costa Rica
// Engineer: David Torres Garc�a
// 
// Create Date: 01/18/2020 07:21:36 PM
// Design Name: 
// Module Name: Mem_Datos_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mem_Datos_tb();
    reg clk, write_enable;  //cable para reloj, write enable en 1 si quiere escribir
    reg [5:0] addr;         //Direccion para accesar (se usa para leer y escribir)
    reg [31:0] din;         //Dato de entrada
    wire [31:0] dout;       //Dato de salida

    //Funci�n para escritura y lectura
    Mem_Datos dut(clk, write_enable, addr, din, dout);   
          
    //Se inicializa el reloj que usa la funci�n
    always
        begin
            clk = 1; #5; clk = 0; #5;   // 10ns por per�odo
        end
    
   // Pruebas escritura y lectura:
   initial begin
   write_enable=1; addr=6'b000000; din=32'h0f00030a;    #10;
   write_enable=0; addr=6'b000000;                      #10;
   if (dout !== 32'h0f00030a) $display("Error de escritura en Addr: 000000");
   
   write_enable=1; addr=6'b000001; din=32'h00b00008;    #10;
   write_enable=0; addr=6'b000001;                      #10;
   if (dout !== 32'h00b00008) $display("Error de escritura en Addr: 000001");
   
   write_enable=1; addr=6'b000010; din=32'h08004300;    #10;
   write_enable=0; addr=6'b000010;                      #10;
   if (dout !== 32'h08004300) $display("Error de escritura en Addr: 000010");
   
   write_enable=1; addr=6'b000011; din=32'haa000bb0;    #10;
   write_enable=0; addr=6'b000011;                      #10;
   if (dout !== 32'haa000bb0) $display("Error de escritura en Addr: 000011");
   
   write_enable=1; addr=6'b000100; din=32'h00a0300a;    #10;
   write_enable=0; addr=6'b000100;                      #10;
   if (dout !== 32'h00a0300a) $display("Error de escritura en Addr: 000100");
   
   write_enable=1; addr=6'b001000; din=32'h10011300;    #10;
   write_enable=0; addr=6'b001000;                      #10;
   if (dout !== 32'h10011300) $display("Error de escritura en Addr: 001000");
   
   write_enable=1; addr=6'b001100; din=32'h00020340;    #10;
   write_enable=0; addr=6'b001100;                      #10;
   if (dout !== 32'h00020340) $display("Error de escritura en Addr: 001100");
   
   write_enable=1; addr=6'b001101; din=32'hff00030f;    #10;
   write_enable=0; addr=6'b001101;                      #10;
   if (dout !== 32'hff00030f) $display("Error de escritura en Addr: 001101");
   
   write_enable=1; addr=6'b100001; din=32'hffffffff;    #10;
   write_enable=0; addr=6'b100001;                      #10;
   if (dout !== 32'hffffffff) $display("Error de escritura en Addr: 100001");
   
   write_enable=1; addr=6'b010010; din=32'h0a00f30b;    #10;
   write_enable=0; addr=6'b010010;                      #10;
   if (dout !== 32'h0a00f30b) $display("Error de escritura en Addr: 010010");
   
   write_enable=1; addr=6'b111111; din=32'h000abc00;    #10;
   write_enable=0; addr=6'b111111;                      #10;
   if (dout !== 32'h000abc00) $display("Error de escritura en Addr: 111111");
   
   write_enable=1; addr=6'b110011; din=32'h00000301;    #10;
   write_enable=0; addr=6'b110011;                      #10;
   if (dout !== 32'h00000301) $display("Error de escritura en Addr: 110011");
   
   write_enable=1; addr=6'b101010; din=32'h00200301;    #10;
   write_enable=0; addr=6'b101010;                      #10;
   if (dout !== 32'h00200301) $display("Error de escritura en Addr: 101010");
   
   write_enable=1; addr=6'b101011; din=32'h0ddd0300;    #10;
   write_enable=0; addr=6'b101011;                      #10;
   if (dout !== 32'h0ddd0300) $display("Error de escritura en Addr: 101011");
   end
   
endmodule
