`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/20/2020 10:27:26 AM
// Design Name: 
// Module Name: registrocsr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module registrocsr(
input write_enable, zeroflag,
output reg out
    );

always @*
begin
    if(write_enable)
        out = zeroflag;
end
     
endmodule
