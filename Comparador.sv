`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/19/2020 06:52:24 PM
// Design Name: 
// Module Name: Comparador
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Comparador #(parameter width = 32)(
    input wire [width-1:0] a,
    input wire [width-1:0] b,
    output reg equal,
    output reg lower,
    output reg greater
    );

    always @* begin
      if (a<b) begin
        equal = 0;
        lower = 1;
        greater = 0;
      end
      else if (a==b) begin
        equal = 1;
        lower = 0;
        greater = 0;
      end
      else begin
        equal = 0;
        lower = 0;
        greater = 1;
      end
    end
endmodule