`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/20/2020 01:22:17 PM
// Design Name: 
// Module Name: EXT_ZERO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EXT_ZERO #(parameter int unsigned size = 32)(
input logic [size-1:0] a, //entrada del inmediato
output logic [31:0] y); //salida extendida

assign y[31:size]='b0; //extencion de ceros
assign y[size-1:0]=a; //asignación de el inmediato

endmodule
