`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.01.2020 20:56:53
// Design Name: 
// Module Name: tb_SLL
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_SLL();
   
reg [31:0] num, shift;
reg [31:0] num_shift;

// instantiate device under test
SLL#(32) dut (num, shift, num_shift);

// 
initial begin
//CASO 1
num = 32'hFFFFFFFF; shift = 32'h00000004; #10;                  // apply input, wait
if (num_shift !== 32'hFFFFFFF0) $display("Caso 1 fallo");         // check
//CASO 2                
shift = 32'hFFFFFFFF; #10;                                // apply input, wait
if (num_shift !== 32'h00000000) $display("Caso 2 fallo");     // check
//CASO 3
num = 32'hFFFFFFFF; shift = 32'h00000000; #10;                // apply input, wait
if (num_shift !== 32'hFFFFFFFF ) $display("Caso 3 fallo");    // check

end
endmodule
