`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/21/2020 05:24:47 PM
// Design Name: 
// Module Name: micro_risc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module micro_risc(
    input clk, reset,
    output [5:0] pc_out, pc_in,
    output [31:0] instr, regread1, regread2, aluresult, mem_instr_out
    );
/////////////////////////////////////////////cables////////////////////////////////
//Cables para PC
wire [5:0] new_pc, salida_pc;

//Cables Memoria de Instrucciones
wire [5:0] mem_instraddr;
wire [31:0] instr_salida;
assign mem_instraddr = salida_pc;

//Cables para Banco de registros
wire [4:0] reg1, reg2, reg_writeaddr;
wire [31:0] reg_out1, reg_out2, reg_datain;

assign reg1 = instr_salida[19:15];
assign reg2 = instr_salida[24:20];
assign reg_writeaddr = instr_salida[11:7];

//wire inmediato
wire [11:0] imm; //inmediato que proviene de la instruccion
wire [31:0] imm_ext; //inmediato extendido
assign imm = instr_salida[31:20];

//wire shamt
wire [4:0] shamt; // proveniente de la instruccion
wire [31:0] shamt_ext; // extendido para que concuerde el bus de salida del mux
assign shamt = instr_salida[24:20];

// wire salida del mux de operandos
wire[31:0] OpB;

//wire resultado de la alu
wire [31:0] Res_ALU;

//Cables para Memoria datos
wire [5:0] mem_datosaddr;
wire [31:0] mem_datain,mem_dataout;

assign mem_datosaddr = Res_ALU[5:0];
assign mem_datain = reg_out2;

//Cables CSR
wire csrin, csrout;
wire [31:0] csrEXT;

//Cables sumador
wire [5:0] suma_res,uno;
assign uno = 6'b000_001;

//Cables mux de pc
wire [5:0] entimm;
assign entimm[5] = 1'b0;
assign entimm[4:0] = instr_salida[11:7];
/////////////////////////////////para la unidad de control//////////////////////////////
//cable para branch
wire branch_taken;

//selector mux_operandos
wire [1:0]sel_mo;

//selector de operancion de la alu
wire [2:0] sel_alu;

//bandera de cero
wire zero;

//selector mux writeback
//wire [1:0] sel_wb;
wire sel_wb;

//selector mux csr
wire [1:0] sel_csr;

//selector mux pc
wire sel_pc;

//write enable mem datos
wire mem_writeenable;

//write enable CSR
wire csr_we;
assign csr_we = 1;
//write enable banco reg
wire reg_writeenable;
////////////////////////////////////////////salidas de modulomo///////////////////////////////
assign pc_out = salida_pc;
assign pc_in = new_pc;
assign instr = instr_salida;
assign regread1 = reg_out1;
assign regread2 = reg_out2;
assign aluresult = Res_ALU;
assign mem_instr_out = mem_dataout;
////////////////////////////////////////////modulos///////////////////////////////////////////
UnidadControl Control (instr_salida[6:0],instr_salida[14:12],reg_writeenable,sel_mo,sel_alu,,mem_writeenable,sel_wb, branch_taken);

assign sel_pc = branch_taken & zero; 

ADD #(6) sumador_pc (salida_pc, uno, suma_res);
reg_PC programCounter (clk, reset, new_pc, salida_pc);
Mem_Inst MemInstrucciones(mem_instraddr, instr_salida);
RegFile bancoRegistros (clk, reg_writeenable, reg1, reg2, reg_writeaddr, reg_datain, reg_out1, reg_out2);
Mem_Datos MemoriaDatos (clk, mem_writeenable, mem_datosaddr, mem_datain, mem_dataout);
registrocsr CSR (csr_we, zero, csrout);    


//Muxancho1_4a1 Mux_csr (zero,reg_out1[0], reg1[0],'b0,sel_csr,csrin);
Mux2_1 #(6) Mux_pc (suma_res,entimm,sel_pc,new_pc );
EXT_ZERO #(12) Extceroimm (imm,imm_ext); // extensor de cero del inmediato
EXT_ZERO #(5) Extceroshamt (shamt, shamt_ext); // extensor de cero del shift amount
EXT_ZERO #(1) ExtceroCSR (csrout,csrEXT);
Mux4_1 Mux_operandos (reg_out2, imm_ext, shamt_ext,'b0, sel_mo, OpB); //mux de eleccion del operando B
ALU #(32) alu(sel_alu, reg_out1, OpB, Res_ALU, zero); // Modulo de la ALU
Mux2_1 #(32) Mux_writback (Res_ALU, mem_dataout, sel_wb, reg_datain);
    
endmodule
