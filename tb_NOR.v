`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.01.2020 22:04:43
// Design Name: 
// Module Name: tb_NOR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_NOR();
reg [31:0] a, b;
wire  y;

// instantiate device under test
NOR#(32) dut (a, b, y);

// 
initial begin
//CASO 1
a = 32'h11111111; b = 32'h11111111; #10;                   // apply input, wait
if (y !== 0) $display("Caso 1 fallo");     // check
//CASO 2                
b = 32'h00000000; #10;                                // apply input, wait
if (y !== 0) $display("Caso 2 fallo");     // check
//CASO 3
a = 32'h00000000; b = 32'h00000000; #10;                   // apply input, wait
if (y !== 1 ) $display("Caso 3 fallo");    // check

end
endmodule
