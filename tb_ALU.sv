`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.01.2020 00:01:52
// Design Name: 
// Module Name: tb_ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_ALU();
reg [2:0]alucontrol;
reg [31:0] a, b;
reg [31:0]resultado;
reg zero;

// instantiate device under test
ALU#(32) dut (alucontrol, a, b, resultado, zero);

// 
initial begin
//CASO 1 (AND)
a = 32'h11111111; b = 32'h11111111;
alucontrol = 3'b000; #10;                   // apply input, wait
if (resultado !== 'h11111111) $display("Caso 1 fallo");     // check
if (zero !== 0) $display("Bandera de Zero activa en caso 1");     // check

//CASO 2 (SLTU)               
a = 32'h00000000; b = 32'h11111111;
alucontrol = 3'b110; #10;                   // apply input, wait
if (resultado !== 'h00000001) $display("Caso 2 fallo");     // check
if (zero !== 0) $display("Bandera de Zero activa en caso 2");     // check

//CASO 3 (ADD)
a = 32'h00000000; b = 32'h00000000;
alucontrol = 3'b011; #10;                   // apply input, wait
if (resultado !== 'h00000000) $display("Caso 3 fallo");     // check
if (zero !== 0) $display("Bandera de Zero activa en caso 3");     // check

end

endmodule
