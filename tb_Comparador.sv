`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.01.2020 22:33:00
// Design Name: 
// Module Name: tb_Comparador
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_Comparador();
reg [31:0] a, b;
reg equal, lower, greater;

// instantiate device under test
Comparador#(32) dut (a, b, equal, lower, greater );

// 
initial begin
//CASO 1
a = 32'h00000000; b = 32'h11111111; #10;                   // apply input, wait
if (equal !== 0 ) $display("Caso 1 fallo equal");     // check
if (lower !== 1 ) $display("Caso 1 fallo lower");     // check
if (greater !== 0 ) $display("Caso 1 fallo greater");     // check
//CASO 2                
b = 32'h00000000; #10;                                // apply input, wait
if (equal !== 1 ) $display("Caso 2 fallo equal");     // check
if (lower !== 0 ) $display("Caso 2 fallo lower");     // check
if (greater !== 0 ) $display("Caso 2 fallo greater");     // check
//CASO 3
a = 32'h11111111; #10;                   // apply input, wait
if (equal !== 0 ) $display("Caso 3 fallo equal");     // check
if (lower !== 0 ) $display("Caso 3 fallo lower");     // check
if (greater !== 0 ) $display("Caso 3 fallo greater");     // check

end
endmodule
