`timescale 10ns/1ps

module control_tb();
	reg clk,rst, p_error;
	reg [5:0] cmd_in;
	wire aluin_reg_en, datain_reg_en, aluout_reg_en, invalid_data;
	reg [1:0] in_select_a, in_select_b;
	wire [3:0] opcode;
	
	control DUT(clk,rst,p_error,cmd_in, aluin_reg_en, datain_reg_en, aluout_reg_en, 
				invalid_data, in_select_a, in_select_b, opcode);
	
	initial begin
	clk = 0;
	p_error = 0;
	rst = 1;
	cmd_in = 6'b000000;
	#10
	rst = 0;
	#10
	cmd_in = 6'b011101;
	#10
	cmd_in = 6'b101010;
	#10
	cmd_in = 6'b110111;
	#10
	rst = 1;
	cmd_in = 6'b010100;
	#10
	$finish;
	end

	initial begin
	forever #1 clk = ~clk;	
	end

	initial begin
	$dumpvars;
    $display (" cmd_in | aluin_reg_en |datain_reg_en |aluout_reg_en |in_select_a |in_select_b |opcode");
    $monitor (" %b |      %b       |      %b       |       %b      |      %b    |     %b     |  %d ", cmd_in, aluin_reg_en, 				datain_reg_en, aluout_reg_en, in_select_a, in_select_b, opcode);
	end

endmodule
