`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/19/2020 08:03:27 PM
// Design Name: 
// Module Name: NOR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NOR#(parameter width = 32)(
input logic [width-1:0] a,
input logic [width-1:0] b,
output logic y);

logic [width-1:0] yl;
genvar i;
generate

    for(i=0; i<width; i=i+1) begin: forloop
        assign yl[i] = ~(a[i] | b[i]);
    end
endgenerate

assign y = yl & 'b1;




endmodule