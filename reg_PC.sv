`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/20/2020 10:11:55 AM
// Design Name: 
// Module Name: reg_PC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reg_PC(
    input clk, reset,
    input logic [5:0] in,
    output logic [5:0] out
    );

always @ (posedge clk)
begin
 if (reset)
  out = 6'b000_000;
 else
    out = in; 
end
    
endmodule
